# Gaia-X Framework

Visual representation of the Gaia-X Framework.

## For developers

### Install
`npm install`

### Dev
`npm run dev`

### Build
`npm run build`

### Data
All the artefacts information are in src/data/, to simply add a software' version, edit this JSON file

### Using
- Vite / React / Typescript
- Chakra UI 
- Intro Js