export type ReleaseType = {
  version: string
  links: string[]
}

export type ReleasesType = {
  [key: string]: ReleaseType | ReleaseType[]
}

export type GroupType = { name: string; url: string }

export type ArtefactType = {
  name: string
  mandatory?: boolean
  sections: string[]
  groups?: Array<GroupType>
  releases?: ReleasesType
}

export type SoftwareArtefactType = {
  id: string
  name: string
  type: string
  mandatory?: boolean
  links: Record<string, { document: string; repository: string; runningEndpoint: string }>
}

export type PillarsHighlightType = 'Compliance' | 'Federation' | 'Data-Exchange'

export type DeliversHighlightType =
  | 'Functional Specifications'
  | 'Technical Specifications'
  | 'Rules Specifications'
  | 'Labels'
  | 'Open Source Software'
  | 'Specifications'
