import React, { useEffect } from 'react'
import './SpecificationsV2.css'
import LeaderLine from 'leader-line-new'

export const SpecificationsV2 = () => {
  useEffect(() => {
    const lines: LeaderLine[] = []
    const default_options = { color: '#B900FF', size: 8 }
    lines.push(
      new LeaderLine(document.getElementById('gx_technical_compatibility')!, document.getElementById('specs')!, {
        ...default_options,
        startLabel: 'Is obtained using'
      })
    )
    lines.push(
      new LeaderLine(document.getElementById('gx_compliance')!, document.getElementById('labels')!, {
        ...default_options,
        startLabel: 'Is obtained using'
      })
    )
    lines.push(
      new LeaderLine(document.getElementById('gx_endorsement')!, document.getElementById('lhp')!, {
        ...default_options,
        startLabel: 'Becomes by using'
      })
    )
    lines.push(
      new LeaderLine(document.getElementById('tck')!, document.getElementById('interop_sem_tech')!, { ...default_options, startLabel: 'Enabler for' })
    )
    lines.push(
      new LeaderLine(document.getElementById('gxdch')!, document.getElementById('interop_legal_org')!, {
        ...default_options,
        startLabel: 'Enabler for'
      })
    )
    lines.push(
      new LeaderLine(document.getElementById('politics')!, document.getElementById('marketing')!, { ...default_options, startLabel: 'Enabler for' })
    )
    // lines.push(
    //   new LeaderLine(document.getElementById('gx_technical_compatibility')!, document.getElementById('gx_endorsement')!, {
    //     color: 'blue',
    //     dash: true,
    //     endSocket: 'right'
    //   })
    // )
    return () => {
      lines.forEach(line => {
        line.remove()
      })
    }
  }, [])

  return (
    <div id="main">
      <div id="sections" style={{ marginLeft: '2em' }}>
        <div id={'notions'}>
          <div className="title">what : Gaia-X Standard</div>
          <div id="gx_technical_compatibility" className="cell">
            Gaia-X Technical Compatibility
          </div>
          <div id="gx_compliance" className="cell">
            Gaia-X Compliance
          </div>
          <div id="gx_endorsement" className="cell">
            Gaia-X Endorsed Project
          </div>
        </div>
        <div id={'deliverables'}>
          <div className="title">Gaia-X deliverables</div>
          <div id={'tf'}>
            <div id={'trustframework'}>
              <div id="specs" className="cell">
                <b>Gaia-X Technical Compatibility Specs</b> 📚
                <ul>
                  <li>Gaia-X Architecture document</li>
                  <li>Gaia-X Data Exchange document</li>
                  <li>Gaia-X Identity, Credential and Access Management document</li>
                  <li>Gaia-X Ontology</li>
                </ul>
              </div>
              <div id="labels" className="cell">
                <b>Gaia-X Compliance document</b> 📚
                <ul>
                  <li>Gaia-X Labels Standard Compliance and Level 1 to Level 3</li>
                  <li>
                    Applicable to ICT Services, Data Exchange Services, and Participants <span style={{ fontStyle: 'italic' }}>(soon)</span>
                  </li>
                </ul>
              </div>
              <div className="cell_no_border">➕</div>
              <div className="cell_no_border">➕</div>
              <div id="tck" className="cell">
                <b>Technical Compatibility Kit 🧰</b> (TCK)
                <br />&<br />
                <b>Reference Software Toolkit</b>
              </div>
              <div id="gxdch" className="cell">
                Gaia-X Digital Clearing House 🤖 (GXDCH)
              </div>
            </div>
          </div>

          <div id="lhp" className="cell">
            <b>Gaia-X Endorsement Program</b> 📚 for
            <ul>
              <li>Gaia-X Qualified Project</li>
              <li>Gaia-X Lighthouse Project</li>
              <li>Gaia-X Lighthouse Data Space</li>
            </ul>
          </div>
          <div className="cell_no_border">➕</div>
          <div id="politics" className="cell">
            <b>Assessment</b> based on endorsement criteria 🤝
          </div>
        </div>
        <div id={'values'}>
          <div className="title">business value</div>
          <div id="interop_sem_tech" className="cell">
            Semantic and Technical interoperability
          </div>
          <div id="interop_legal_org" className="cell">
            Legal and Organisational interoperability
          </div>
          <div id="marketing" className="cell">
            Digital ecosystem / data space with clear and measurable governance rules
          </div>
        </div>
      </div>
    </div>
  )
}
