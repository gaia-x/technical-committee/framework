import React from 'react'
import { Divider, Flex, Link, Text } from '@chakra-ui/react'

export const Footer: React.FC = () => {
  return (
    <Flex direction={'column'} mt={5} w={'100%'}>
      <Divider orientation={'horizontal'} />
      <Text fontSize={12} mt={3}>
        ©2023 Gaia-X European Association for Data and Cloud AISBL
      </Text>
      <Text fontSize={12}>
        Source available at{' '}
        <Link color="blue" href={'https://gitlab.com/gaia-x/technical-committee/framework'}>
          https://gitlab.com/gaia-x/technical-committee/framework
        </Link>
      </Text>
      <Text fontSize={12} mt={1}>
        Those documents are protected by copyright law and international treaties. You may download, print or electronically view those documents for
        your personal or internal company (or company equivalent) use. You are not permitted to adapt, modify, republish, print, download, post or
        otherwise reproduce or transmit those documents, or any part of it, for a commercial purpose without the prior written permission of Gaia-X
        European Association for Data and Cloud AISBL. No copying, distribution, or use other than as expressly provided herein is authorized by
        implication, estoppel or otherwise. All rights not expressly granted are reserved.
      </Text>
    </Flex>
  )
}
