import React, { useState } from 'react'
import { ArtefactType } from '../../types'
import {
  Button,
  Flex,
  Link,
  ListItem,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  StackDivider,
  Text,
  UnorderedList,
  useDisclosure,
  VStack
} from '@chakra-ui/react'
import { capitalize } from '../../utils/capitalize'
import { ArtefactBgColors } from '../../utils/calculateArtefactBgColor'

interface ArtefactProps {
  artefact: ArtefactType
  index: number
  bgColors: ArtefactBgColors
}

export const Artefact: React.FC<ArtefactProps> = ({ artefact, bgColors }) => {
  const { name } = artefact

  const { isOpen, onOpen, onClose } = useDisclosure()
  const [previousReleaseIsOpen, setPreviousReleaseIsOpen] = useState<boolean>(false)

  const getWidth = () => {
    switch (name) {
      case 'Architecture document':
        return '650px'
      case 'Trust Framework':
        return '100%'
      default:
        return '200px'
    }
  }

  return (
    <Flex
      color={'white'}
      width={getWidth()}
      maxWidth={'75%'}
      height={'75px'}
      justifyContent={'center'}
      alignItems={'center'}
      borderRadius={'5px'}
      textAlign={'center'}
      boxShadow={'md'}
      p={2}
      bgColor={bgColors.bg}
      bgGradient={bgColors.bg}
      _hover={{
        bgGradient: bgColors.hover,
        cursor: 'pointer'
      }}
      onClick={onOpen}
    >
      <Text fontSize={'13'} fontWeight={'bold'}>
        {name}
      </Text>
      <Modal isOpen={isOpen} onClose={onClose} isCentered>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader color={'secondary'}>{name}</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <VStack alignItems={'flex-start'} divider={<StackDivider borderColor="gray.200" />}>
              {artefact.releases && (
                <Flex direction={'column'} className={'releases'}>
                  <Text fontWeight={'bold'}>Release(s)</Text>
                  <UnorderedList pl={3} mt={1}>
                    {Object.entries(artefact.releases).map(([label, release], i) =>
                      Array.isArray(release) ? (
                        <ListItem key={label}>
                          <Text cursor={'pointer'} onClick={() => setPreviousReleaseIsOpen(!previousReleaseIsOpen)}>
                            {capitalize(label)} {previousReleaseIsOpen ? '▲' : '▼'}
                          </Text>
                          <UnorderedList>
                            {previousReleaseIsOpen &&
                              release.map((r, i) => (
                                <ListItem key={label + i}>
                                  <Link color={'blue'} href={r.links[0]}>
                                    {r.version}
                                  </Link>
                                </ListItem>
                              ))}
                          </UnorderedList>
                        </ListItem>
                      ) : (
                        <ListItem key={label + i}>
                          <Text>
                            {capitalize(label)}(
                            <Link color={'blue'} href={release.links[0]}>
                              {release.version}
                            </Link>
                            )
                          </Text>
                        </ListItem>
                      )
                    )}
                  </UnorderedList>
                </Flex>
              )}
              {artefact.groups && (
                <Flex direction={'column'} className={'groups'}>
                  <Text fontWeight={'bold'}>Group(s)</Text>
                  <UnorderedList pl={3} mt={1}>
                    {artefact.groups.map((group, i) => (
                      <ListItem key={group.name + i}>
                        <Link color={'blue'} href={group.url}>
                          {group.name}
                        </Link>
                      </ListItem>
                    ))}
                  </UnorderedList>
                </Flex>
              )}
              {artefact.sections && (
                <Flex direction={'column'} className={'sections'}>
                  <Text fontWeight={'bold'}>Section(s)</Text>
                  <UnorderedList pl={3} mt={1}>
                    {artefact.sections.map((section, index) => (
                      <ListItem key={index}>
                        <Text>{section}</Text>
                      </ListItem>
                    ))}
                  </UnorderedList>
                </Flex>
              )}
            </VStack>
          </ModalBody>

          <ModalFooter>
            <Button bgGradient={'linear(to-r, #AF00FA 10%, #110094 55%)'} colorScheme={'facebook'} mr={3} onClick={onClose}>
              Close
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </Flex>
  )
}
