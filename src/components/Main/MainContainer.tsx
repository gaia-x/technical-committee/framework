import React from 'react'
import { ArtefactsGrid } from './ArtefactsGrid'
import { Flex } from '@chakra-ui/react'

interface MainContainerProps {
  setActiveTab: React.Dispatch<React.SetStateAction<number>>
}

export const MainContainer: React.FC<MainContainerProps> = ({ setActiveTab }) => {
  return (
    <Flex direction={'column'} className={'maintab'}>
      <ArtefactsGrid setActiveTab={setActiveTab} />
    </Flex>
  )
}
