import React from 'react'
import { SpecificationsV2 } from '../SpecificationsV2/SpecificationsV2'

interface ArtefactsGridProps {
  setActiveTab: React.Dispatch<React.SetStateAction<number>>
}

export const ArtefactsGrid: React.FC<ArtefactsGridProps> = () => {
  return <SpecificationsV2 />
}
