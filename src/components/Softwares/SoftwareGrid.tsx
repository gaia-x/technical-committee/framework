import React, { Fragment } from 'react'
import { Box, Center, Flex, Grid, GridItem, Heading, Link, Text, useTheme } from '@chakra-ui/react'
import { softwares } from '../../data/softwares.json'
import { SoftwareArtefact } from './SoftwareArtefact'
import { SoftwareArtefactType } from '../../types'

interface SoftwareGridProps {
  mandatoryIsChecked: boolean
  version: string
}

export const SoftwareGrid: React.FC<SoftwareGridProps> = ({ mandatoryIsChecked, version }) => {
  const theme = useTheme()

  const pillarsProps: Record<string, string> = {
    Compliance: '1 / 1 / 11 / 2',
    Federation: '1 / 2 / 11 / 3',
    'Data Exchange': '1 / 3 / 11 / 4'
  }

  const pillarsNameProps: Record<string, string> = {
    'Compliance-heading': '1 / 1 / 2 / 2',
    'Federation-heading': '1 / 2 / 2 / 3',
    'Data Exchange-heading': '1 / 3 / 2 / 4'
  }

  const softwarePlacements: Record<string, string> = {
    compliance: '2 / 1 / 3 / 2',
    registry: '3 / 1 / 4 / 2',
    'continuous-automated-monitoring': '2  / 2 / 3 / 3',
    orchestration: '3 / 2 / 4 / 3',
    'personal-credential-manager': '4 / 2 / 5 / 3',
    'organization-credential-manager': '5 / 2 / 6 / 3',
    'trust-services-api': '6 / 2 / 7 / 3',
    'notarization-api': '7 / 2 / 8 / 3',
    'core-catalogue-functions': '8 / 2 / 9 / 3',
    'self-description-wizard': '9 / 2 / 10 / 3',
    'data-exchange-logging-service': '2 / 3 / 3 / 4',
    'data-contract-services': '3 / 3 / 4 / 4',
    'data-connector': '4 / 3 / 5 / 4',
    testbed: '10 / 1 / 11 / 4'
  }

  const pillars = Object.keys(pillarsProps)

  return (
    <Flex direction={'column'} minW={'840px'} w={'100%'}>
      <Text p={3} m={3}>
        Set of software artefacts developed by our Open Source Community to create, manage, operate federations as well as services to achieve Gaia-X
        Compliance.
        <br />
        Engage with our{'  '}
        <Link href="https://gitlab.com/gaia-x/gaia-x-community/open-source-community" isExternal textDecoration="underline">
          OSS community here
        </Link>
        .
      </Text>
      <Grid templateRows={'repeat(10, 1fr)'} templateColumns={'repeat(3, auto)'} w={'100%'}>
        {softwares.map(artefact => (
          <GridItem key={artefact.name} gridArea={softwarePlacements[artefact.id]}>
            <Center mt={5}>
              <Box w={'60%'}>
                <SoftwareArtefact artefact={artefact as SoftwareArtefactType} mandatoryIsChecked={mandatoryIsChecked} version={version} />
              </Box>
            </Center>
          </GridItem>
        ))}
        <GridItem gridArea={softwarePlacements['testbed']}>
          <Center mt={5}>
            <Box w={'90%'}>
              <SoftwareArtefact
                artefact={{
                  id: 'testbed',
                  name: 'Gaia-X Validation Testbed (to be developed)',
                  type: '',
                  mandatory: false,
                  links: {}
                }}
                mandatoryIsChecked={mandatoryIsChecked}
                version={version}
              />
            </Box>
          </Center>
        </GridItem>
        {pillars.map(pillar => (
          <Fragment key={pillar}>
            <GridItem key={pillar} gridArea={pillarsProps[pillar]} mx={12}>
              <Box
                boxSize={'100%'}
                minW={'300px'}
                pt={4}
                bgGradient={`linear(to-b, ${theme.colors.primary},rgb(158, 203, 230),rgb(255,255, 255))`}
                borderTopLeftRadius={'xl'}
                borderTopRightRadius={'xl'}
              />
            </GridItem>
            <GridItem gridArea={pillarsNameProps[`${pillar}-heading`]}>
              <Center w={'100%'} h={'100%'} mt={3}>
                <Heading textAlign={'center'} size={'lg'} color={'white'}>
                  {pillar}
                </Heading>
              </Center>
            </GridItem>
          </Fragment>
        ))}
      </Grid>
    </Flex>
  )
}
