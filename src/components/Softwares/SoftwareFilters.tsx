import React from 'react'
import { Checkbox, Divider, Flex, Heading, Radio, RadioGroup, Stack, VStack } from '@chakra-ui/react'
import { getVersions } from '../../utils/getVersions'

interface SoftwareFiltersProps {
  setMandatoryIsChecked: React.Dispatch<React.SetStateAction<boolean>>
  setVersion: React.Dispatch<React.SetStateAction<string>>
}

export const SoftwareFilters: React.FC<SoftwareFiltersProps> = ({ setMandatoryIsChecked, setVersion }) => {
  const versions = getVersions()

  return (
    <Flex w={'20%'} minW={'280px'}>
      <Flex direction={'column'} w={'100%'}>
        <Heading size={'lg'} color={'primary'}>
          Filters
        </Heading>
        <Flex direction={'column'} border={'2px solid #110094'} p={3} w={'90%'} position={'relative'} color={'primary'} mt={10}>
          <Heading size={'xs'} position={'absolute'} top={'-8.5px'} backgroundColor={'white'} px={1}>
            Mandatory
          </Heading>
          <VStack align={'flex-start'} py={3}>
            <Checkbox name={'Mandatory'} onChange={e => setMandatoryIsChecked(e.target.checked)}>
              Mandatory
            </Checkbox>
          </VStack>
        </Flex>
        <Flex mt={8} direction={'column'}>
          <Heading size={'md'} color={'primary'}>
            Versions
          </Heading>
          <RadioGroup mt={5} onChange={setVersion}>
            <Stack>
              {versions.map(version => (
                <Radio key={version} value={version}>
                  {version}
                </Radio>
              ))}
            </Stack>
          </RadioGroup>
        </Flex>
      </Flex>
      <Divider orientation={'vertical'} mr={7} />
    </Flex>
  )
}
