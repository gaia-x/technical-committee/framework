import React from 'react'
import { Divider, Link, Text, VStack } from '@chakra-ui/react'

interface ClearingHouseServiceColumnProps {
  name: string
  version: string
  status: string
  endpoint: string
  index?: number
}

export const ClearingHouseServiceColumn: React.FC<ClearingHouseServiceColumnProps> = ({ name, version, status, endpoint, index }) => {
  return (
    <VStack w={'33%'} className={`service-${name}-${index}`}>
      <Text color={'secondary'} fontWeight={'bold'}>
        {name}
      </Text>
      <Divider />
      <Link href={endpoint} isExternal fontWeight={'bold'} color={'blue.600'} textDecoration={'underline'}>
        {version}
      </Link>
      <Divider />
      <Text fontWeight={'bold'}>{status}</Text>
    </VStack>
  )
}
