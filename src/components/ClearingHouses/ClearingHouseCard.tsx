import React from 'react'
import { Card, CardBody, CardHeader, Center, Heading, HStack } from '@chakra-ui/react'
import { ClearingHouse } from './ClearingHouseContainer'
import { ClearingHouseServiceColumn } from './ClearingHouseServiceColumn'
import { overrideClearingHouseName } from '../../utils/overrideClearingHouseName'
import { ClearingHouseCredential } from './ClearingHouseCredential'

export const ClearingHouseCard: React.FC<{
  clearingHouse: ClearingHouse
  index?: number
}> = ({ clearingHouse, index }) => {
  return (
    <Card
      borderColor={'primary'}
      borderWidth={1}
      minW={'320px'}
      maxW={'400px'}
      w={'100%'}
      boxShadow={'xl'}
      className={`clearing-house-card-${index}`}
    >
      <CardHeader mb={'0'} pb={'0'}>
        <Center>
          <Heading size={'lg'} mb={'0'} color={'primary'}>
            {overrideClearingHouseName(clearingHouse.name)}
          </Heading>
          <ClearingHouseCredential clearingHouseName={clearingHouse.name} />
        </Center>
      </CardHeader>
      <CardBody pt={'0'}>
        <HStack gap={0}>
          <ClearingHouseServiceColumn
            name={'Compliance'}
            version={clearingHouse.complianceEndpoint.version || 'N/A'}
            status={clearingHouse.complianceEndpoint.version ? 'UP' : 'DOWN'}
            endpoint={clearingHouse.complianceEndpoint.endpoint}
            index={index}
          />
          <ClearingHouseServiceColumn
            name={'Registry'}
            version={clearingHouse.registryEndpoint.version || 'N/A'}
            status={clearingHouse.registryEndpoint.version ? 'UP' : 'DOWN'}
            endpoint={clearingHouse.registryEndpoint.endpoint}
          />
          <ClearingHouseServiceColumn
            name={'Notary'}
            version={clearingHouse.registrationNotaryEndpoint.version || 'N/A'}
            status={clearingHouse.registrationNotaryEndpoint.version ? 'UP' : 'DOWN'}
            endpoint={clearingHouse.registrationNotaryEndpoint.endpoint}
          />
        </HStack>
      </CardBody>
    </Card>
  )
}
