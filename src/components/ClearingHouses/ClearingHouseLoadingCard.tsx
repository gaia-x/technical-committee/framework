import React from 'react'
import { Card, CardBody, CardHeader, Center, Heading, SkeletonText } from '@chakra-ui/react'
import { overrideClearingHouseName } from '../../utils/overrideClearingHouseName'

type ClearingHouseLoadingCardProps = {
  index: number
  clearingHouseName: string
}
export const ClearingHouseLoadingCard: React.FC<ClearingHouseLoadingCardProps> = ({ index, clearingHouseName }) => {
  return (
    <Card key={'card' + index} borderColor={'primary'} borderWidth={1} minW={'320px'} maxW={'400px'} w={'100%'} boxShadow={'xl'}>
      {' '}
      <CardHeader mb={'0'} pb={'0'}>
        <Center>
          <Heading size={'lg'} mb={'0'} color={'primary'}>
            {overrideClearingHouseName(clearingHouseName)}
          </Heading>
        </Center>
      </CardHeader>
      <CardBody pt={'0'}>
        <SkeletonText mt="4" noOfLines={4} spacing="4" skeletonHeight="2" />
      </CardBody>
    </Card>
  )
}
