import React, { useEffect, useMemo, useState } from 'react'
import { Box, Center, Flex, Grid, GridItem, Heading, Link, Spinner, Text, useToast } from '@chakra-ui/react'
import { useQuery } from 'react-query'
import { ClearingHouseCard } from './ClearingHouseCard'
import { ExternalLinkIcon } from '@chakra-ui/icons'
import { ClearingHouseOverview } from './ClearingHouseOverview'
import { ClearingHouseLoadingCard } from './ClearingHouseLoadingCard'
import { overrideClearingHouseName } from '../../utils/overrideClearingHouseName'

type Endpoint = {
  endpoint: string
  version?: string
}

export type ClearingHouse = {
  name: string
  complianceEndpoint: Endpoint
  registryEndpoint: Endpoint
  registrationNotaryEndpoint: Endpoint
}

const fetchTrustedIssuers = async () => {
  const url = 'https://registry.lab.gaia-x.eu/v1/api/trusted-issuers'
  const response = await fetch(url)

  // Check if the response is okay, otherwise throw an error
  if (!response.ok) {
    throw new Error(`HTTP error! Status: ${response.status}`)
  }

  return await response.json()
}

export const ClearingHouseContainer: React.FC = () => {
  const toast = useToast()
  const [clearingHouseList, setClearingHouseList] = useState<ClearingHouse[]>([])
  const [clearingHousesState, setClearingHousesState] = useState<ClearingHouse[]>([])

  const [clearingHousesStateLoaded, setClearingHousesStateLoaded] = useState<Map<string, boolean>>(new Map())

  const { data, isFetched } = useQuery({
    queryKey: ['clearingHouses'],
    queryFn: fetchTrustedIssuers,
    onError: () =>
      toast({
        title: 'Error',
        description: 'Error while fetching Clearing Houses',
        status: 'error',
        duration: 9000,
        isClosable: true
      }),
    initialData: {
      registry: [],
      compliance: [],
      'registration-notary': []
    },
    refetchOnWindowFocus: false,
    enabled: true,
    refetchInterval: 60000 // 1 minute,
  })

  const clearingHouseProviders = useMemo(() => {
    const getDomainName = (clearingHouseBaseUrl: string): string => {
      const host = new URL('https://' + clearingHouseBaseUrl).host
      if (host.startsWith('registry') || host.startsWith('gx-registry')) {
        return host.slice(host.indexOf('.') + 1)
      }
      return host
    }

    const urls: string[] = Object.values(data)[0] as string[]
    return urls
      .map(getDomainName)
      .filter(providers => providers.indexOf('lab.gaia-x.eu') === -1)
      .sort((a, b) => {
        return overrideClearingHouseName(a).localeCompare(overrideClearingHouseName(b))
      })
  }, [data])

  useEffect(() => {
    const findEndpoint = (data: string[], provider: string) => {
      let endpoint = data.find((endpoint: string) => endpoint.includes(provider))

      if (endpoint) {
        endpoint = endpoint + '/docs'
      }

      return endpoint ?? ''
    }

    const clearingHouses: ClearingHouse[] = clearingHouseProviders.map((clearingHouseProvider: string) => ({
      name: clearingHouseProvider,
      complianceEndpoint: {
        endpoint: 'https://' + findEndpoint(data.compliance, clearingHouseProvider)
      },
      registryEndpoint: {
        endpoint: 'https://' + findEndpoint(data.registry, clearingHouseProvider)
      },
      registrationNotaryEndpoint: {
        endpoint: 'https://' + findEndpoint(data['registration-notary'], clearingHouseProvider)
      }
    }))
    setClearingHouseList(clearingHouses)
  }, [clearingHouseProviders])

  useMemo(() => {
    const getVersion = async (clearingHouseBaseUrl: string): Promise<string | undefined> => {
      const fetchVersion = async () => {
        const response = await fetch(clearingHouseBaseUrl.replace('/docs', ''), {
          method: 'GET',
          headers: {
            'Content-Type': 'application/json'
          }
        })
        const data = await response.json()
        return data.version
      }

      try {
        return await fetchVersion()
      } catch (error) {
        console.error('An error has occurred while fetching the version from the clearing house ' + error)
        return undefined
      }
    }

    const clearingHousesStateLoaded: Map<string, boolean> = new Map()
    const clearingHousesStateArray: ClearingHouse[] = []
    clearingHouseList
      .map(async (clearingHouse: ClearingHouse) => ({
        ...clearingHouse,
        complianceEndpoint: {
          ...clearingHouse.complianceEndpoint,
          version: await getVersion(clearingHouse.complianceEndpoint.endpoint)
        },
        registryEndpoint: {
          ...clearingHouse.registryEndpoint,
          version: await getVersion(clearingHouse.registryEndpoint.endpoint)
        },
        registrationNotaryEndpoint: {
          ...clearingHouse.registrationNotaryEndpoint,
          version: await getVersion(clearingHouse.registrationNotaryEndpoint.endpoint)
        }
      }))
      .forEach(async clearingHouseWithVersionPromise => {
        const clearingHouseWithVersion = await clearingHouseWithVersionPromise
        clearingHousesStateLoaded.set(clearingHouseWithVersion.name, true)
        setClearingHousesStateLoaded(new Map(clearingHousesStateLoaded))
        clearingHousesStateArray.push(clearingHouseWithVersion)
        setClearingHousesState(clearingHousesStateArray)
      })
  }, [clearingHouseList])

  const getOperationalClearingHousesCount = () => {
    const opCount = clearingHousesState.filter(
      clearingHouse =>
        clearingHouse.complianceEndpoint.version && clearingHouse.registryEndpoint.version && clearingHouse.registrationNotaryEndpoint.version
    ).length

    return opCount === clearingHousesState.length ? 'ALL' : `${opCount} / ${clearingHousesState.length} `
  }

  return (
    <Flex className={'status-page'} mt={5} w={'100%'} direction={{ base: 'column', md: 'row' }}>
      <ClearingHouseOverview
        providers={clearingHouseProviders}
        clearingHouses={clearingHousesState}
        operationalClearingHousesCount={getOperationalClearingHousesCount()}
      />
      <Flex direction={'column'} ml={{ base: 0, md: 10 }} mt={{ base: 6, md: 0 }} w={'100%'}>
        <Box w={'100%'}>
          <Heading size={'2xl'} color={'primary'}>
            GXDCH STATUS
          </Heading>
          <Text ml={2} mt={1}>
            This page indicates whether a service is UP and running. It does not attest to the end to end functionality
          </Text>
        </Box>
        <Box className={'clearing-houses-grid'} mt={10} borderWidth={1} borderColor={'secondary'} borderRadius={'md'} px={10} py={10} w={'100%'}>
          <Grid
            w={'100%'}
            templateColumns={{
              base: 'repeat(1, 1fr)',
              md: 'repeat(3, 1fr)'
            }}
            gap={10}
          >
            {isFetched &&
              clearingHouseList.map((clearingHouse, index) =>
                clearingHousesStateLoaded.get(clearingHouse.name) ? (
                  <GridItem key={clearingHouse.name}>
                    <ClearingHouseCard clearingHouse={clearingHousesState.find(cls => cls.name === clearingHouse.name)!} index={index} />
                  </GridItem>
                ) : (
                  <GridItem key={clearingHouse.name}>
                    <ClearingHouseLoadingCard index={index} clearingHouseName={clearingHouse.name}></ClearingHouseLoadingCard>
                  </GridItem>
                )
              )}
            {!isFetched && (
              <Center w={'100%'}>
                <Spinner size={'xl'} />
              </Center>
            )}
          </Grid>
        </Box>
        <Box width="100%" height="100%" mt={10} borderWidth={1} borderColor={'secondary'} borderRadius={'md'} px={10} py={10} w={'100%'}>
          <iframe
            src="https://mongxdch.azimov.gxdch.eu/dashboard.html"
            title="External Dashboard"
            width="100%"
            height="5000hv"
            style={{ border: 0 }}
            allowFullScreen
          />
        </Box>
        <Link mt={2} href={'https://gaia-x.eu/gxdch/'} isExternal>
          Learn more about Gaia-X Clearing House <ExternalLinkIcon ml="2px" />
        </Link>
      </Flex>
    </Flex>
  )
}
