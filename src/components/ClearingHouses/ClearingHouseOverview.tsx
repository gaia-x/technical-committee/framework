import React, { useState } from 'react'
import {
  Box,
  Button,
  Center,
  Flex,
  Image,
  Link,
  ListItem,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Text,
  UnorderedList,
  useDisclosure
} from '@chakra-ui/react'
import opLogo from '../../assets/oplogo.png'
import { ClearingHouse } from './ClearingHouseContainer'
import { ClearingHouseCard } from './ClearingHouseCard'
import { overrideClearingHouseName } from '../../utils/overrideClearingHouseName'

export const ClearingHouseOverview: React.FC<{
  providers: string[]
  clearingHouses: ClearingHouse[]
  operationalClearingHousesCount: string
}> = ({ providers, clearingHouses, operationalClearingHousesCount }) => {
  const { isOpen, onOpen, onClose } = useDisclosure()
  const [selectedClearingHouse, setSelectedClearingHouse] = useState<ClearingHouse>()

  const handleSelectClearingHouse = (clearingHouseName: string) => {
    const selectedClearingHouse = clearingHouses.find(ch => ch.name === clearingHouseName)
    if (selectedClearingHouse) {
      setSelectedClearingHouse(selectedClearingHouse)
      onOpen()
    }
  }

  return (
    <Flex direction={'column'}>
      <Center borderWidth={1} borderColor={'gray'} borderRadius={'md'} py={2} px={5}>
        <Flex direction={'column'} alignItems={'center'} w={'80%'} textAlign={'center'}>
          {operationalClearingHousesCount === 'ALL' && <Image src={opLogo} boxSize={'50px'} />}
          <Text mt={2} fontWeight={'bold'}>
            {operationalClearingHousesCount} SYSTEMS OPERATIONAL
          </Text>
        </Flex>
      </Center>
      <Box w={'100%'} mt={5} borderWidth={1} borderColor={'gray'} borderRadius={'md'} py={2} px={5} className={'overview'}>
        <Text>Overview</Text>
        <UnorderedList ml={12} mt={3}>
          {providers.map(clearingHouseProvider => (
            <ListItem key={clearingHouseProvider} onClick={() => handleSelectClearingHouse(clearingHouseProvider)}>
              <Link textDecoration={'underline'}>{overrideClearingHouseName(clearingHouseProvider)}</Link>
            </ListItem>
          ))}
        </UnorderedList>
      </Box>
      {selectedClearingHouse && (
        <Modal isOpen={isOpen} onClose={onClose} isCentered>
          <ModalOverlay />
          <ModalContent>
            <ModalHeader>Gaia-X Digital Clearing House</ModalHeader>
            <ModalCloseButton />
            <ModalBody>
              <ClearingHouseCard clearingHouse={selectedClearingHouse} />
            </ModalBody>

            <ModalFooter>
              <Button colorScheme="blue" mr={3} onClick={onClose}>
                Close
              </Button>
            </ModalFooter>
          </ModalContent>
        </Modal>
      )}
    </Flex>
  )
}
