import React from 'react'
import { CheckCircleIcon, InfoIcon, QuestionIcon, TimeIcon } from '@chakra-ui/icons'
import { useEffect, useState } from 'react'
import { Modal, ModalBody, ModalCloseButton, ModalContent, ModalHeader, ModalOverlay, Tooltip, useDisclosure } from '@chakra-ui/react'

export interface ClearingHouseCredentialProps {
  clearingHouseName: string
}

const vcURL: { [clearingHouseName: string]: string } = {
  'www.delta-dao.com': 'https://www.delta-dao.com/.well-known/2210_gx_participant.json',
  'gxdch.dih.telekom.com': 'https://dih.telekom.com/.well-known/2210_gx_participant_vp.json',
  'aerospace-digital-exchange.eu': 'https://aerospace-digital-exchange.eu/.well-known/2210_gx_participant_neusta_aerospace_gmbh.json',
  'arsys.es': 'https://arlabdevelopments.com/.well-known/gaia-x-loire/vcParticipantArsysInternet.json'
}

enum VerifiedStateEnum {
  UNVERIFIED = 'UNVERIFIED',
  VERIFIED = 'VERIFIED',
  PENDING = 'PENDING',
  UNVERIFIABLE = 'UNVERIFIABLE'
}

export const ClearingHouseCredential = ({ clearingHouseName }: ClearingHouseCredentialProps) => {
  const [isVerified, setIsVerified] = useState<string>(VerifiedStateEnum.PENDING)
  const [verificationVC, setVerificationVC] = useState<object | undefined>()
  const { isOpen, onToggle, onClose } = useDisclosure()

  useEffect(() => {
    const loadVC = async () => {
      if (!vcURL[clearingHouseName]) {
        setIsVerified(VerifiedStateEnum.UNVERIFIABLE)
        return
      }
      const vcResponse = await fetch(vcURL[clearingHouseName])
      if (!vcResponse.ok) {
        setIsVerified(VerifiedStateEnum.UNVERIFIED)
        return
      }
      const vc = await vcResponse.text()
      const complianceResponse = await fetch('https://compliance.gaia-x.eu/v1/api/credential-offers', {
        body: vc,
        method: 'POST'
      })
      if (complianceResponse.status !== 201) {
        setIsVerified(VerifiedStateEnum.UNVERIFIED)
        return
      }
      setIsVerified(VerifiedStateEnum.VERIFIED)
      setVerificationVC(await complianceResponse.json())
    }
    loadVC()
  }, [])
  return (
    <>
      <div style={{ marginLeft: '0.5rem' }}>
        {isVerified === VerifiedStateEnum.VERIFIED && (
          <Tooltip hasArrow label="GXDCH provider identity compliant with Gaia-X. Click to see Compliance VC">
            <CheckCircleIcon
              color={'green'}
              onClick={() => {
                onToggle()
              }}
            />
          </Tooltip>
        )}
        {isVerified === VerifiedStateEnum.UNVERIFIED && (
          <Tooltip hasArrow label="GXDCH provider identity compliance verification failed">
            <InfoIcon color={'orange'} />
          </Tooltip>
        )}
        {isVerified === VerifiedStateEnum.PENDING && (
          <Tooltip hasArrow label="GXDCH provider identity check in progress">
            <TimeIcon />
          </Tooltip>
        )}
        {isVerified === VerifiedStateEnum.UNVERIFIABLE && (
          <Tooltip hasArrow label="GXDCH provider credentials not provided. Unable to verify identity">
            <QuestionIcon />
          </Tooltip>
        )}
      </div>
      <Modal isOpen={isOpen} size={'5xl'} onClose={onClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader fontWeight="semibold">Verification VC</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <pre style={{ whiteSpace: 'break-spaces' }}>{JSON.stringify(verificationVC, null, 2)}</pre>
          </ModalBody>
        </ModalContent>
      </Modal>
    </>
  )
}
