import { DeliversHighlightType, PillarsHighlightType } from '../types'

export const deliversFilters: DeliversHighlightType[] = ['Specifications', 'Open Source Software', 'Labels']

export const specifications: DeliversHighlightType[] = ['Technical Specifications', 'Rules Specifications', 'Functional Specifications']

export const pillarsFilters: PillarsHighlightType[] = ['Compliance', 'Federation', 'Data-Exchange']

export const isPillar = (elm: string): elm is PillarsHighlightType => {
  return pillarsFilters.includes(elm as PillarsHighlightType)
}

export const isDeliver = (elm: string): elm is DeliversHighlightType => {
  return deliversFilters.includes(elm as DeliversHighlightType) || specifications.includes(elm as DeliversHighlightType)
}
