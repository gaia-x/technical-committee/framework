import { capitalize } from './capitalize'

export const overrideClearingHouseName = (name: string) => {
  switch (name) {
    case 'lab.gaia-x.eu':
      return 'Gaia-X Lab'
    case 'gxdch.dih.telekom.com':
      return 'T-Systems'
    case 'airenetworks.es':
      return 'Aire Networks'
    case 'aruba.it':
      return 'Aruba'
    case 'arsys.es':
      return 'Arsys'
    case 'www.delta-dao.com':
      return 'deltaDAO'
    case 'gxdch.gaiax.ovh':
      return 'OVHCloud'
    case 'aerospace-digital-exchange.eu':
      return 'Neusta Aerospace'
    case 'gxdch.proximus.eu':
      return 'Proximus'
    default:
      return capitalize(name)
  }
}
