import { softwares } from '../data/softwares.json'

export const getVersions = (): string[] =>
  Array.from(new Set(softwares.map(software => Object.keys(software.links)).flat(1)))
    .sort(compareVersions)
    .reverse()

function compareVersions(a: string, b: string) {
  const [aMajor, aMinor] = a.split('.').map(part => part.split('-')[0])
  const [bMajor, bMinor] = b.split('.').map(part => part.split('-')[0])

  if (parseInt(aMajor) !== parseInt(bMajor)) {
    return parseInt(aMajor) - parseInt(bMajor)
  }

  return parseInt(aMinor) - parseInt(bMinor)
}
