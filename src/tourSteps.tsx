import React from 'react'
import deliverablesImg from './assets/deliverables.png'
import scopesImg from './assets/scopes.png'
import { StepType } from '@reactour/tour'

export interface CustomStep extends StepType {
  relatedTab: 0 | 1 | 2
}

export const steps: CustomStep[] = [
  // -------------- GENERIC --------------
  {
    relatedTab: 0,
    selector: '.chakra-tabs',
    content: () => (
      <div>
        Hello 👋 <br />
        Welcome in this quick tour of the <b>Gaia-X Framework</b> !<br />
        You can use the ⬅️ and ➡️ from your keyboard.
        <br />
        <b>Best experience on a FullHD screen in full screen (F11)</b>
      </div>
    )
  },
  {
    selector: '.tabs',
    content: () => (
      <>
        <b>Gaia-X Framework</b> <br /> This is a matrix representation of all the Gaia-X artefacts and how they relate to each other.
      </>
    ),
    relatedTab: 0
  },
  {
    selector: '#gx_technical_compatibility',
    highlightedSelectors: ['#gx_technical_compatibility', '#specs', '#tck'],
    content: () => (
      <>
        <b>Gaia-X Technical compatibility</b>
        <br />
        The deliverables in this column enable the <i>semantic</i> and <i>technical</i> interoperability for the ecosystems.{' '}
        <img src={scopesImg} width="250px" alt={'the Gaia-X ecosystems in the form of an X '} />
      </>
    ),
    relatedTab: 0
  },
  {
    selector: '#gx_compliance',
    highlightedSelectors: ['#gx_compliance', '#labels', '#gxdch'],
    content: () => (
      <>
        <b>Gaia-X Compliance</b>
        <br />
        The deliverables in this column enable the <i>legal</i> and <i>operational</i> interoperability for the ecosystems.
      </>
    ),
    relatedTab: 0
  },
  {
    selector: '#specs',
    highlightedSelectors: ['#specs', '#labels'],
    content: () => (
      <>
        The deliverables in this row are the <b>Gaia-X Specifications</b> 📄
        <br />
        <img src={deliverablesImg} width="250px" alt={'deliverables with three blocks, specs, code and labels'} />
      </>
    ),
    relatedTab: 0
  },
  {
    selector: '#tck',
    highlightedSelectors: ['#tck', '#gxdch'],
    content: () => (
      <>
        Operationalisation
        <br />
        The GXDCH, TCK and reference software 📦 are the operationalization of the Gaia-X Trust Framework, enabling usage of the{' '}
        <b>Gaia-X Trust Framework</b>
        <br />
        <img src={deliverablesImg} width="250px" alt={'deliverables with three blocks, specs, code and labels'} />
      </>
    ),
    relatedTab: 0
  },
  {
    selector: '.clearing-houses-grid',
    content: () => (
      <>
        GXDCH status page
        <br />
        Welcome to the <strong>Gaia-X Digital Clearing House (GXDCH) status page</strong>. This interface offers a <strong>real-time overview</strong>{' '}
        of various services, ensuring you're always informed about their operational health.
      </>
    ),
    relatedTab: 2
  },
  {
    selector: '.overview',
    content: () => (
      <>
        GXDCH overview
        <br />
        Explore the <strong>GXDCH</strong> overview to <strong>access the health status of different Clearing Houses</strong>. Click on any Clearing
        House for a detailed status card.
      </>
    ),
    relatedTab: 2
  },
  {
    selector: '.clearing-houses-grid',
    content: () => (
      <>
        GXDCH grid
        <br />
        The <strong>GXDCH grid</strong> provides a systematic display of Clearing Houses.{' '}
        <strong>Each card offers essential details about its state and version running.</strong>
      </>
    ),
    relatedTab: 2
  },
  {
    selector: '.clearing-house-card-0',
    content: () => (
      <>
        GXDCH status card
        <br />
        Here's a <strong>snapshot of a Clearing House's operational status</strong>. <br />
        It provides insights into <strong>service uptimes</strong> and potential interruptions.
      </>
    ),
    relatedTab: 2
  },
  {
    selector: '.service-Compliance-0',
    content: () => (
      <>
        GXDCH service infos
        <br />
        This section delivers <strong>information about a specific service</strong>. Click on the version number for a deeper dive into its{' '}
        <strong>API documentation.</strong>
      </>
    ),
    relatedTab: 2
  },
  {
    selector: 'body',
    content: () => (
      <>
        Thank you ! 💕
        <br />
        Your turn to browse this first knowledge base.
        <br />
        If you have any question, please reach out to <a href="mailto:info@gaia-x.eu">info@gaia-x.eu</a> or{' '}
        <a href="https://gitlab.com/gaia-x/technical-committee/framework" target="_blank" rel="noreferrer">
          Gitlab
        </a>
      </>
    ),
    relatedTab: 0
  }
]
