import { Header } from './components/Header'
import { Flex, Tab, TabList, TabPanel, TabPanels, Tabs } from '@chakra-ui/react'
import { MainContainer } from './components/Main/MainContainer'
import React, { useState } from 'react'
import { SoftwareContainer } from './components/Softwares/SoftwareContainer'
import { Footer } from './components/Footer'
import { ClearingHouseContainer } from './components/ClearingHouses/ClearingHouseContainer'
import { steps } from './tourSteps'
import { TourProvider } from '@reactour/tour'

const tabs = ['specifications', 'software', 'clearing-house']

function App() {
  // get the url query param tab
  const urlParams = new URLSearchParams(window.location.search)
  const tab = urlParams.get('tab')

  const [activeTab, setActiveTab] = useState<number>(tab ? tabs.indexOf(tab) : 0)

  const handleTabChange = (index: number) => {
    setActiveTab(index)
    const query = tabs[index]
    history.pushState(null, '', `?tab=${query}`)
  }

  return (
    <div className="App">
      <TourProvider
        steps={steps}
        disableInteraction={true}
        disableKeyboardNavigation={true}
        disableDotsNavigation={true}
        prevButton={() => `toto`}
        showCloseButton={false}
        showNavigation={false}
      >
        <Flex direction={'column'} p={8}>
          <Header setActiveTab={setActiveTab} />
          <Tabs isLazy index={activeTab} onChange={handleTabChange} variant="enclosed" size={'lg'} className={'tabs'}>
            <TabList color={'primary'}>
              <Tab ml={'5%'}>Specifications</Tab>
              <Tab className={'software-tab'}>Software</Tab>
              <Tab>Clearing House</Tab>
            </TabList>
            <TabPanels>
              <TabPanel>
                <MainContainer setActiveTab={setActiveTab} />
              </TabPanel>
              <TabPanel>
                <SoftwareContainer />
              </TabPanel>
              <TabPanel>
                <ClearingHouseContainer />
              </TabPanel>
            </TabPanels>
          </Tabs>
          <Footer />
        </Flex>
      </TourProvider>
    </div>
  )
}

export default App
