import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  base: './',
  build: {
    rollupOptions: {
      output: {
        manualChunks: {
          'chakra-react-context': ['@chakra-ui/react-context'],
          'chakra-react': ['@chakra-ui/react']
        }
      }
    }
  }
})
